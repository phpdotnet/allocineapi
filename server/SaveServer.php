<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 6/15/15
 * Time: 8:03 PM
 */

include 'Worker.php';

$user = 'root';
$password = 'root';
$url = 'mysql:host=localhost;dbname=AllocineAPIDB';

$connection = new PDO($url, $user, $password);

$worker = new Worker($connection);
$worker->run();
