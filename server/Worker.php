<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 6/15/15
 * Time: 11:46 AM
 */


require_once __DIR__.'/../vendor/autoload.php';
use Phalcon\Queue\Beanstalk;


class Worker {
    protected $connection;
    private $queue = null;

    public function __construct($connection) {
        $queue = new Beanstalk(array('host' => '0.0.0.0'));
        $queue->connect();
        $this->queue = $queue;
        $this->connection = $connection;
/*
        $this->connection = new Mysql(array(
            'adapter'     => 'Mysql',
            'host'        => '127.0.0.1',
            'username'    => 'root',
            'password'    => 'root',
            'dbname'      => 'AllocineAPIDB',
            'charset'     => 'utf8',
            ));
*/
    }

    public function run() {
        while (($job = $this->queue->reserve())) {
            if ($job == null)
                continue;

            $body = $job->getBody();
            $key = key($body);
            if ($key == 'film')
                $this->saveFilm($body[$key]);
            else if ($key == 'serie')
                $this->saveSerie($body[$key]);
            else if ($key == 'star')
                $this->saveStar($body[$key]);
            $job->delete();
        }
    }

    private function saveFilm($id)
    {
        $redis = new \Predis\Client();
        $film = json_decode($redis->get($id), true);

        //Insert Media
        $phsql = "INSERT INTO Media (id, name, synopsis, photo, date_out, press_score, viewer_score)
                  VALUES (:id, :name, :synopsys, :photo, :date, :press_score, :viewer_score);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $id, 'name' => $film['title'],
            'synopsys' => $film['synopsys'], 'photo' => $film['photo'],
            'date' => $film['date'], 'press_score' => $film['press_score'], 'viewer_score' => $film['viewer_score']));

        //Insert Film
        $phsql = "INSERT INTO Film(id_media, duration) VALUE (:id, :duration);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $id, 'duration' => $film['duration']));

        //Insert Type
        $types = $film['types'];
        $phsql = "INSERT INTO Is_Type (id_media, type) VALUES";
        $bind_parameter = array('id_media' => $id);
        for ($i = 0; $i < count($types); $i += 1) {
            if ($i != 0)
                $phsql .= ", ";
            $phsql .= "(:id_media, :type_$i)";
            $bind_parameter["type_$i"] = $types[$i];
        }
        $response = $this->connection->prepare($phsql);
        $response->execute($bind_parameter);

        //Insert Casting
        $casting = $film['casting'];

        //Realisator
        $phsql = "INSERT INTO Person (id, name, birth_date, biography, photo)
                  VALUE (:id, :name, :birth_date, :biography, :photo);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $casting['realisator']['id'], 'name' => $casting['realisator']['name'],
            'birth_date' => null, 'biography' => null, 'photo' => $casting['realisator']['photo']));

        //Link the realisator to the movie
        $phsql = "INSERT INTO Realisation (id_media, id_person) VALUE (:id_media, :id_person);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id_media' => $id, 'id_person' => $casting['realisator']['id']));

        $actors = $casting['actors'];
        $phsql_p = "INSERT INTO Person (id, name, birth_date, biography, photo) VALUES";
        $phsql_a = "INSERT INTO Act_In (id_person, id_media, role) VALUES";
        for ($i = 0; $i < count($actors); $i += 1) {
            $bind_parameter = array();
            $bind_parameter["id"] = $actors[$i]['id'];
            $bind_parameter["name"] = $actors[$i]['name'];
            $bind_parameter["date"] = null;
            $bind_parameter["bio"] = null;
            $bind_parameter["photo"] = $actors[$i]['photo'];
            $phsql = $phsql_p;
            $phsql .= "(:id, :name, :date, :bio, :photo)";

            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);

            $bind_parameter = array('id_media' => $id);
            $phsql = $phsql_a;
            $phsql .= "(:id, :id_media, :role)";

            $bind_parameter["id"] = $actors[$i]['id'];
            $bind_parameter["role"] = $actors[$i]['role'];

            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);
        }

        //Insert Teasers
        $teasers = $film['teasers'];
        if (count($teasers) > 0) {
            $phsql = "INSERT INTO Teaser(id, name, url, photo, id_media) VALUES";
            $bind_parameter = array('id' => $id);

            for ($i = 0; $i < count($teasers); $i += 1) {
                $teaser = $teasers[$i];
                if ($i != 0)
                    $phsql .= ", ";
                $phsql .= "(:id_$i, :name_$i, :url_$i, :photo_$i, :id)";
                $bind_parameter["id_$i"] = $teaser['id'];
                $bind_parameter["name_$i"] = $teaser['name'];
                $bind_parameter["url_$i"] = $teaser['url'];
                $bind_parameter["photo_$i"] = $teaser['photo'];
            }
            $phsql .= ';';
            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);
        }
    }

    private function saveSerie($id) {
        $redis = new \Predis\Client();
        $serie = json_decode($redis->get($id), true);

        //Insert Media
        $phsql = "INSERT INTO Media (id, name, synopsis, photo, date_out, press_score, viewer_score)
                  VALUES (:id, :name, :synopsys, :photo, :date, :press_score, :viewer_score);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $id, 'name' => $serie['title'],
            'synopsys' => $serie['synopsys'], 'photo' => $serie['photo'],
            'date' => $serie['date'], 'press_score' => $serie['press_score'], 'viewer_score' => $serie['viewer_score']));

        //Insert serie
        $phsql = "INSERT INTO Serie(id_media, format) VALUE (:id, :format);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $id, 'format' => $serie['format']));

        //Insert Type
        $types = $serie['types'];
        $phsql = "INSERT INTO Is_Type (id_media, type) VALUES";
        $bind_parameter = array('id_media' => $id);
        for ($i = 0; $i < count($types); $i += 1) {
            if ($i != 0)
                $phsql .= ", ";
            $phsql .= "(:id_media, :type_$i)";
            $bind_parameter["type_$i"] = $types[$i];
        }
        $response = $this->connection->prepare($phsql);
        $response->execute($bind_parameter);

        //Insert Casting
        $casting = $serie['casting'];
        //Creator
        $phsql = "INSERT INTO Person (id, name, birth_date, biography, photo)
                  VALUES (:id, :name, :birth_date, :biography, :photo);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $casting['creator']['id'], 'name' => $casting['creator']['name'],
            'birth_date' => null, 'biography' => null, 'photo' => $casting['creator']['photo']));

        //Link the realisator to the movie
        $phsql = "INSERT INTO Realisation (id_media, id_person) VALUES (:id_media, :id_person);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id_media' => $id, 'id_person' => $casting['creator']['id']));

        $actors = $casting['actors'];
        $phsql_p = "INSERT INTO Person (id, name, birth_date, biography, photo) VALUES";
        $phsql_a = "INSERT INTO Act_In (id_person, id_media, role) VALUES";
        for ($i = 0; $i < count($actors); $i += 1) {
            $bind_parameter = array();
            $bind_parameter["id"] = $actors[$i]['id'];
            $bind_parameter["name"] = $actors[$i]['name'];
            $bind_parameter["date"] = null;
            $bind_parameter["bio"] = null;
            if (isset($actors[$i]['photo']))
                $bind_parameter["photo"] = $actors[$i]['photo'];
            else
                $bind_parameter["photo"] = 'Unknown';
            $phsql = $phsql_p;
            $phsql .= "(:id, :name, :date, :bio, :photo)";

            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);

            $bind_parameter = array('id_media' => $id);
            $phsql = $phsql_a;
            $phsql .= "(:id, :id_media, :role)";

            $bind_parameter["id"] = $actors[$i]['id'];
            if (isset($actors[$i]['role']))
                $bind_parameter["role"] = $actors[$i]['role'];
            else
                $bind_parameter["role"] = 'Unknown';

            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);
        }

        //Insert Teasers
        $teasers = $serie['teasers'];
        if (count($teasers) > 0) {
            $phsql = "INSERT INTO Teaser(id, name, url, photo, id_media) VALUES";
            $bind_parameter = array('id' => $id);

            for ($i = 0; $i < count($teasers); $i += 1) {
                $teaser = $teasers[$i];
                if ($i != 0)
                    $phsql .= ", ";
                $phsql .= "(:id_$i, :name_$i, :url_$i, :photo_$i, :id)";
                $bind_parameter["id_$i"] = $teaser['id'];
                $bind_parameter["name_$i"] = $teaser['name'];
                $bind_parameter["url_$i"] = $teaser['url'];
                $bind_parameter["photo_$i"] = $teaser['photo'];
            }
            $phsql .= ';';
            $response = $this->connection->prepare($phsql);
            $response->execute($bind_parameter);
        }
    }

    private function saveStar($id) {
        $redis = new \Redis\Client();
        $star = json_decode($redis->get($id));

        // Insert PERSON
        $phsql = "INSERT INTO Person (id, name, birth_date, biography, photo)
                  VALUE (:id, :name, :birth_date, :biography, :photo);";
        $response = $this->connection->prepare($phsql);
        $response->execute(array('id' => $star['id'], 'name' => $star['name'],
            'birth_date' => $star['birthdate'], 'biography' => $star['bio'], 'photo' => $star['photo']));
    }
}