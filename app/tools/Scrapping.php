<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 6/10/15
 * Time: 11:54 AM
 */

use Goutte\Client;


class Scrapping {

    public function __construct() {}

    private static function startsWith($haystack, $needle) {
        return $needle === "" or strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    private static function endsWith($haystack, $needle) {
        return $needle === "" or (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

    //Search http://www.allocine.fr/recherche/?q=
    public static function scrappingSearchAll($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/?q='.$search);
        $crawler = $crawler->filter('.colcontent .vmargin10t');

        $result = array();

        $titles = $crawler->filter('.titlebar');
        $tables = $crawler->filter('table');

        $i_table = 0;
        for ($i = 0; $i < $titles->count(); $i += 1) {
            $title = trim($titles->eq($i)->text(), "\n");

            if ($title == 'Films') {
                $table = $tables->eq($i_table)->filter('tr');
                $i_table += 1;

                $films = [];
                for ($j = 0; $j < $table->count(); $j += 2) {
                    $film = array();
                    $tr = $table->eq($j);
                    try {
                        $film['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
                    } catch (Exception $e) {
                        $film['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
                    }
                    $film['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
                    $film['photo'] = $tr->filter('img')->attr('src');
                    $infos = $tr->filter('.fs11')->html();
                    $infos = explode('<br>', $infos);

                    for ($k = 0; $k < count($infos); $k += 1) {
                        $info = trim($infos[$k], "\n\"");
                        if (Scrapping::startsWith($info, 'de')) {
                            $film['realisator'] = substr($info, 3);
                        } else if (Scrapping::startsWith($info, 'avec')) {
                            $info = substr($info, 5);
                            $film['actors'] = explode(', ', $info);
                        } else if (intval($info) != 0) {
                            $film['date'] = date_create_from_format('Y', $info);
                        }
                    }

                    $films[] = $film;
                }
                $result['films'] = $films;
            } else if ($title == 'Lieux, villes, quartiers') {

            } else if ($title == 'Séries TV') {
                $table = $tables->eq($i_table)->filter('tr');
                $i_table += 1;

                $series = [];
                for ($j = 0; $j < $table->count(); $j += 2) {
                    $serie = array();
                    $tr = $table->eq($j);
                    try {
                        $serie['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
                    } catch (Exception $e) {
                        $serie['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
                    }
                    $serie['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
                    $serie['photo'] = $tr->filter('img')->attr('src');
                    $infos = $tr->filter('.fs11')->html();
                    $infos = explode('<br>', $infos);

                    for ($k = 0; $k < count($infos); $k += 1) {
                        $info = trim($infos[$k], "\n\"");
                        if (Scrapping::startsWith($info, 'de')) {
                            $serie['realisator'] = substr($info, 3);
                        } else if (Scrapping::startsWith($info, 'avec')) {
                            $info = substr($info, 5);
                            $serie['actors'] = explode(', ', $info);
                        } else if (intval($info) != 0) {
                            $serie['date'] = date_create_from_format('Y', $info);
                        }
                    }

                    $series[] = $serie;
                }
                $result['series'] = $series;
            } else if ($title == 'Stars') {
                $table = $tables->eq($i_table)->filter('tr');
                $i_table += 1;

                $stars = [];
                for ($j = 0; $j < $table->count(); $j += 2) {
                    $star = array();
                    $tr = $table->eq($j);
                    try {
                        $star['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
                    } catch (Exception $e) {
                        $star['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
                    }
                    $star['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
                    $star['photo'] = $tr->filter('img')->attr('src');
                    $infos = $tr->filter('.fs11')->html();
                    $infos = explode(', ', $infos);
                    $works = [];
                    for ($k = 0; $k < count($infos); $k += 1) {
                        $info = trim($infos[$k], "\n\"");
                        $info = html_entity_decode(str_replace("&nbsp;", '', htmlentities($info)));
                        $info = str_replace("<br>", '', $info);
                        $info = trim($info, "\n");
                        $works[] = $info;
                    }
                    $star['works'] = $works;

                    $stars[] = $star;
                }
                $result['stars'] = $stars;
            } else if ($title == 'Vidéos') {
                $table = $tables->eq($i_table)->filter('tr');
                $i_table += 1;

                $videos = [];
                for ($j = 0; $j < $table->count(); $j += 2) {
                    $video = array();
                    $tr = $table->eq($j);
                    try {
                        $video['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
                    } catch (Exception $e) {
                        $video['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
                    }
                    $video['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
                    $video['photo'] = $tr->filter('img')->attr('src');

                    $videos[] = $video;
                }
                $result['videos'] = $videos;
            } else if ($title == 'Photos') {
                $table = $tables->eq($i_table)->filter('tr');
                $i_table += 1;

                $photos = [];
                for ($j = 0; $j < $table->count(); $j += 2) {
                    $photo = array();
                    $tr = $table->eq($j);
                    try {
                        $photo['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
                    } catch (Exception $e) {
                        $photo['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
                    }
                    $photo['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
                    $photo['photo'] = $tr->filter('img')->attr('src');

                    $photos[] = $photo;
                }
                $result['photos'] = $photos;
            }
        }

        return $result;
    }

    public static function scrappingSearchCelibrity($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/5/?q='.$search);

        $stars = [];
        $rows = $crawler->filter('.vmargin10t table tr');
        for ($i = 0; $i < $rows->count(); $i += 2) {
            $table = $rows->eq($i);
            $star = array();
            $tr = $table->eq($i);
            try {
                $star['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
            } catch (Exception $e) {
                $star['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
            }
            $star['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
            $star['photo'] = $tr->filter('img')->attr('src');
            $infos = $tr->filter('.fs11')->html();
            $infos = explode(', ', $infos);
            $works = [];
            for ($k = 0; $k < count($infos); $k += 1) {
                $info = trim($infos[$k], "\n\"");
                $info = html_entity_decode(str_replace("&nbsp;", '', htmlentities($info)));
                $info = str_replace("<br>", '', $info);
                $info = trim($info, "\n");
                $works[] = $info;
            }
            $star['works'] = $works;

            $stars[] = $star;
        }

        return $stars;
    }

    public static function scrappingSearchFilm($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/1/?q='.$search);

        $films = [];
        $rows = $crawler->filter('.vmargin10t table tr');

        for ($i = 0; $i < $rows->count(); $i += 2) {
            $film = array();
            $tr = $rows->eq($i);
            try {
                $film['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
            } catch (Exception $e) {
                $film['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
            }
            $film['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
            $film['photo'] = $tr->filter('img')->attr('src');
            $infos = $tr->filter('.fs11')->html();
            $infos = explode('<br>', $infos);

            for ($k = 0; $k < count($infos); $k += 1) {
                $info = trim($infos[$k], "\n\"");
                if (Scrapping::startsWith($info, 'de')) {
                    $film['realisator'] = substr($info, 3);
                } else if (Scrapping::startsWith($info, 'avec')) {
                    $info = substr($info, 5);
                    $film['actors'] = explode(', ', $info);
                } else if (intval($info) != 0) {
                    $film['date'] = intval($info);
                }
            }
            $films[] = $film;
        }
        return $films;
    }

    public static function scrappingSearchSerie($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/6/?q='.$search);

        $series = [];
        $rows = $crawler->filter('.vmargin10t table tr');
        for ($i = 0; $i < $rows->count(); $i += 1) {
            $serie = array();
            $tr = $rows->eq($i);
            try {
                $serie['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
            } catch (Exception $e) {
                $serie['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
            }
            $serie['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
            $serie['photo'] = $tr->filter('img')->attr('src');
            $infos = $tr->filter('.fs11')->html();
            $infos = explode('<br>', $infos);

            for ($k = 0; $k < count($infos); $k += 1) {
                $info = trim($infos[$k], "\n\"");
                if (Scrapping::startsWith($info, 'de')) {
                    $serie['realisator'] = substr($info, 3);
                } else if (Scrapping::startsWith($info, 'avec')) {
                    $info = substr($info, 5);
                    $serie['actors'] = explode(', ', $info);
                } else if (intval($info) != 0) {
                    $serie['date'] = intval($info);
                }
            }
            $series[] = $serie;
        }
        return $series;
    }

    public static function scrappingSearchVideo($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/18/?q='.$search);

        $videos = [];
        $rows = $crawler->filter('.vmargin10t table tr');
        for ($i = 0; $i < $rows->count(); $i += 2) {
            $video = array();
            $tr = $rows->eq($i);
            try {
                $video['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
            } catch (Exception $e) {
                $video['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
            }
            $video['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
            $video['photo'] = $tr->filter('img')->attr('src');

            $videos[] = $video;
        }
        return $rows;
    }

    public static function scrappingSearchPhoto($search) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/9/?q='.$search);

        $photos = [];
        $rows = $crawler->filter('.vmargin10t table tr');
        for ($i = 0; $i < $rows->count(); $i += 1) {
            $photo = array();
            $tr = $rows->eq($i);
            try {
                $photo['name'] = trim($tr->filter('td')->eq(1)->filter('a')->text(), "\n");
            } catch (Exception $e) {
                $photo['name'] = trim($tr->filter('td')->eq(1)->filter('span')->text(), "\n");
            }
            $photo['url'] = 'www.allocine.fr'.$tr->filter('a')->attr('href');
            $photo['photo'] = $tr->filter('img')->attr('src');

            $photos[] = $photo;
        }

        return $photos;
    }

    //Fiche http://www.allocine.fr/film/fichefilm_gen_cfilm=$id.html
    public static function scrappingFilmFiche($id)
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/film/fichefilm_gen_cfilm='.$id.'.html');

        $film = array('id' => $id);
        $casting = array();

        //Title
        $film['title'] = trim($crawler->filter('#title > span')->text(), "\n");

        //Photo
        $film['photo'] = $crawler->filter('.poster img')->attr('src');

        //Other information
        $fiche = $crawler->filter('.data_box_table > tbody > tr');
        for ($i = 0; $i < $fiche->count(); $i += 1) {
            $tr = $fiche->eq($i);

            $th = trim($tr->filter('th')->text(), "\n");
            $td = $tr->filter('td');

            if ($th == 'Date de sortie') {
                $spans = $td->filter('span');
                try {
                    $film['date'] = trim($spans->eq(1)->text(), "\n");
                    $film['duration'] = trim($spans->eq(2)->text(), "\n");
                } catch (Exception $e) {
                    //Coming soon
                    $film['date'] = null;
                    $film['duration'] = null;
                }
            }
            //Realisator
            else if ($th == 'Réalisé par') {
                $realisator = array();
                $link_realisator = $td->filter('span > a');
                $realisator['name'] = $link_realisator->attr('title');
                $realisator['url'] = 'www.allocine.fr'.$link_realisator->attr('href');
                $casting['realisator'] = $realisator;
            }
            //Actors
            else if ($th == 'Avec') {
                $actors = [];
                $actors_link = $td->filter('a');
                for ($j = 0; $j < $actors_link->count(); $j += 1) {
                    $actor = array();
                    $actor['name'] = $actors_link->eq($j)->attr('title');
                    $actor['url'] = 'www.allocine.fr'.$actors_link->eq($j)->attr('href');
                    $actors[] = $actor;
                }
                $casting['actors'] = $actors;
            }
            //Origins
            else if ($th == 'Nationalité') {
                $origins = [];
                $origins_span = $td->filter('span');
                for ($j = 0; $j < $origins_span->count(); $j += 1)
                    $origins[] = trim($origins_span->eq($j)->text(), "\n");

                $film['origins'] = $origins;
            }
            //Types
            else if ($th == 'Genre') {
                $types = [];
                $types_span = $td->filter('span > span');
                for ($j = 0; $j < $types_span->count(); $j += 1) {
                    $types[] = trim($types_span->eq($j)->text(), "\n");
                }
                $film['types'] = $types;
            }
            //Score from press
            else if ($th == 'Presse') {
                $film['press_score'] = floatval(str_replace(',', '.', $td->filter('.note')->text()));
            //Score from viewer
            } else if ($th == 'Spectateurs') {
                $film['viewer_score'] = floatval(str_replace(',', '.', $td->filter('.note')->text()));
            }
        }
        if (!isset($film['press_score']))
            $film['press_score'] = 0;
        if (!isset($film['viewer_score']))
            $film['viewer_score'] = 0;
        $film['casting'] = $casting;

        //Scrapping synopsis
        $film['synopsys'] = trim($crawler->filter('.insist + p[itemprop="description"]')->text(), "\n");

        return $film;
    }

    //Casting http://www.allocine.fr/film/fichefilm-$id/casting
    public static function scrappingFilmCasting($id) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/film/fichefilm-'.$id.'/casting/');

        $casting = [];

        $realisator = array();
        $realisator_html = $crawler->filter('li[itemprop="director"]');
        $realisator['photo'] = $realisator_html->filter('img')->attr('src');
        $realisator_html = $realisator_html->filter('a');
        $realisator['name'] = trim($realisator_html->text(), "\n");
        try {
            $realisator['url'] = 'www.allocine.fr' . $realisator_html->attr('href');
            $realisator['id'] = intval(preg_replace('/\D/', '', $realisator['url']));
        } catch (Exception $e) {
            $realisator['url'] = 'Unknown';
            $realisator['id'] = -1;
        }
        $casting['realisator'] = $realisator;

        $actors = [];
        //Important actors
        $important_actors = $crawler->filter('#actors + div + div li');
        for ($i = 0; $i < $important_actors->count(); $i += 1) {
            $actor = array();

            $actor['photo'] = $important_actors->eq($i)->filter('img')->attr('src');
            $link_actor = $important_actors->eq($i)->filter('a');
            try {
                $actor['name'] = trim($link_actor->text(), "\n");
                $actor['url'] = 'www.allocine.fr' . $link_actor->attr('href');
                $actor['id'] = intval(preg_replace('/\D/', '', $actor['url']));
            } catch (Exception $e) {
                $actor['name'] = trim($important_actors->eq($i)->filter('span')->attr('title'));
                $actor['url'] = 'Unknown';
                $actor['id'] = -1;
            }
            try {
                $role = trim($important_actors->eq($i)->filter('p')->eq(1)->text(), "\n");
                $role = substr($role, strpos($role, ':') + 2);
                $actor['role'] = $role;
            } catch (Exception $e) {
                $actor['role'] = 'Unknown';
            }

            $actors[] = $actor;
        }

        //Other actors
        $other_actors = $crawler->filter("#actors + div + div + table tr");
        for ($i = 0; $i < $other_actors->count(); $i += 1) {
            $actor = array();
            $actor['role'] = trim($other_actors->eq($i)->filter('td')->eq(0)->text(), "\n");
            $img_actor = $other_actors->eq($i)->filter('img');
            $actor['name'] = $img_actor->attr('title');
            try {
                $actor['url'] = 'www.allocine.fr'.$other_actors->eq($i)->filter('a')->attr('href');
                $actor['id'] = intval(preg_replace('/\D/', '', $actor['url']));
            } catch (Exception $e) {
                $actor['url'] = 'Unknown';
                $actor['id'] = -1;
            }
            $actor['photo'] = $img_actor->attr('src');

            $actors[] = $actor;
        }

        $casting['actors'] = $actors;
        return $casting;
    }

    //Teaser
    public static function scrappingFilmTeaser($id) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/recherche/18/?q='.$id);

        $teasers = [];

        $row = $crawler->filter('.colcontent > .rubric > .vmargin10t > table > tr');

        for ($i = 0; $i < $row->count(); $i += 2) {
            $teaser = array();

            $video = $row->eq($i)->filter('td a');

            $teaser_name = trim($video->eq(1)->text(), "\n");
            if (strstr($teaser_name, 'Bande-annonce')) {
                $teaser['name'] = $teaser_name;
                try {
                    $teaser['url'] = 'www.allocine.fr' . $video->eq(0)->attr('href');
                    $teaser['id'] = intval(preg_replace('/\D/', '', $teaser['url']));
                } catch (Exception $e) {
                    $teaser['url'] = 'Unknown';
                    $teaser['id'] = -1;
                }
                $teaser['photo'] = $video->eq(0)->filter('img')->attr('src');


                $teasers[] = $teaser;
            }
        }

        return $teasers;
    }


    //Fiche http://www.allocine.fr/series/ficheserie_gen_cserie=$id.html
    public static function scrappingSerieFiche($id)
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/series/ficheserie_gen_cserie='.$id.'.html');
        $serie = array('id' => $id);

        //Title
        $serie['title'] = trim($crawler->filter('#title > span')->text(), "\n");

        $background_style = explode('\'', $crawler->filter('.posterLarge')->attr('style'));
        //Photo
        $serie['photo'] = $background_style[1];

        $casting = array();

        //Other information
        $fiche = $crawler->filter('.data_box_table > tr');
        for ($i = 0; $i < $fiche->count(); $i += 1) {
            $tr = $fiche->eq($i);

            $th = trim($tr->filter('th')->text(), "\n");
            $td = $tr->filter('td');

            //Creator
            if ($th == 'Créée par') {
                $realisator = array();
                $link_realisator = $td->filter('span > a');
                $realisator['name'] = $link_realisator->attr('title');
                $realisator['url'] = 'www.allocine.fr'.$link_realisator->attr('href');
                $realisator['id'] = intval(preg_replace('/\D/', '', $realisator['url']));
                $realisator['photo'] = 'Unknown';
                $casting['creator'] = $realisator;

                $serie['date'] = date_create_from_format('Y', preg_replace('/\D/', '', $td->text()));
            }
            //Actors
            else if ($th == 'Avec') {
                $actors = [];
                $actors_link = $td->filter('span');
                for ($j = 0; $j < $actors_link->count() and $j < 3; $j += 1) {
                    $actor = array();
                    $actor['name'] = $actors_link->eq($j)->attr('title');
                    try {
                        $actor['url'] = 'www.allocine.fr' . $actors_link->eq($j)->attr('href');
                        $actor['id'] = intval(preg_replace('/\D/', '', $actor['url']));
                    } catch(Exception $e) {
                        $actor['url'] = 'Unknown';
                        $actor['id'] = 0;
                    }

                    $actors[] = $actor;
                }
                $casting['actors'] = $actors;
            }
            //Origins
            else if ($th == 'Nationalité') {
                $origins = [];
                $origins_span = $td->filter('span');
                for ($j = 0; $j < $origins_span->count(); $j += 1)
                    $origins[] = $origins_span->eq($j)->text();

                $serie['origins'] = $origins;
            }
            //Types
            else if ($th == 'Genre') {
                $types = [];
                $types_span = $td->filter('span > span');
                for ($j = 0; $j < $types_span->count(); $j += 1) {
                    $types[] = trim($types_span->eq($j)->text(), "\n");
                }
                $serie['types'] = $types;
            }
            //Statut
            else if ($th == 'Statut') {
                $serie['statut'] = trim($td->text(), "\n");
            }
            //Format
            else if ($th == 'Format') {
                $serie['format'] = trim($td->text(), "\n");
            }
            //Score from press
            else if ($th == 'Presse') {
                $serie['press_score'] = floatval(str_replace(',', '.', $td->filter('.note')->text()));
                //Score from viewer
            } else if ($th == 'Spectateurs') {
                $serie['viewer_score'] = floatval(str_replace(',', '.', $td->filter('.note')->text()));
            }
        }
        if (!isset($serie['press_score']))
            $serie['press_score'] = 0;
        if (!isset($serie['viewer_score']))
            $serie['viewer_score'] = 0;

        $serie['casting'] = $casting;

        $seasons = [];
        $seasons_html = $crawler->filter('#col_main > .button_tabs')->filter('a');

        for ($i = 0; $i < $seasons_html->count(); $i += 1) {
            $season = array();
            $season['number'] = intval(explode(' ', $seasons_html->eq($i)->text())[1]);
            $season['url'] = 'www.allocine.fr'.$seasons_html->eq($i)->attr('href');
            $seasons[] = $season;
        }

        $serie['seasons'] = $seasons;

        //Scrapping synopsis
        $serie['synopsys'] = trim($crawler->filter('.titlebar_01 + p[itemprop="description"] + p')->text(), "\n");


        return $serie;
    }

    //Casting
    public static function scrappingSerieCasting($serie_id, $season_id) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/series/ficheserie-'.$serie_id.'/casting/saison-'.$season_id);

        $casting = array();

        $creator = array();
        $link_creator = $crawler->filter('#Creator + div li');
        $creator_img = $link_creator->filter('img');
        $creator['name'] = $creator_img->attr('title');
        $creator['photo'] = $creator_img->attr('src');
        $creator['url'] = 'www.allocine.fr'.$link_creator->filter('a')->attr('href');
        $creator['id'] = intval(preg_replace('/\D/', '', $creator['url']));
        $casting['creator'] = $creator;

        $actors = [];
        //Important actors
        $actors_html = $crawler->filter('#actors + div li');
        for ($i = 0; $i < $actors_html->count(); $i += 1) {
            $actor = array();
            $actor_html = $actors_html->eq($i);

            $actor['photo'] = $actor_html->filter('img')->attr('src');
            $link_actor = $actor_html->filter('a');
            $actor['name'] = trim($link_actor->text(), "\n");
            try {
                $actor['url'] = 'www.allocine.fr' . $link_actor->attr('href');
                $actor['id'] = intval(preg_replace('/\D/', '', $actor['url']));
            } catch (Exception $e) {
                $actor['url'] = 'Unknown';
                $actor['id'] = 0;
            }
            $role = trim($actor_html->filter('p')->text(), "\n");
            $role = substr($role, strpos($role, ':') + 2);
            $actor['role'] = $role;
            $actors[] = $actor;
        }

        //Other actors
        $other_actors = $crawler->filter("#actors + div + div table tr");
        for ($i = 0; $i < $other_actors->count(); $i += 1) {
            $actor = array();
            $actor['role'] = trim($other_actors->eq($i)->filter('td')->eq(0)->text(), "\n");
            $img_actor = $other_actors->eq($i)->filter('img');
            $actor['name'] = $img_actor->attr('title');
            try {
                $actor['url'] = 'www.allocine.fr'.$other_actors->eq($i)->filter('a')->attr('href');
                $actor['id'] = intval(preg_replace('/\D/', '', $actor['url']));
            } catch (Exception $e) {
                $actor['url'] = 'Unknown';
                $actor['id'] = 0;
            }
            $actor['photo'] = $img_actor->attr('src');

            $actors[] = $actor;
        }

        $casting['actors'] = $actors;

        return $casting;
    }

    //Teaser
    public static function scrappingSerieTeaser($id, $season=0) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/series/ficheserie-'.$id.'/videos');

        $teasers = array();

        $i = 1;
        $season_html = $crawler->filter('#seriesseasonnumber'.$i.' + section article > figure');
        while ($season_html->count()) {
            $season_teaser = [];

            for ($j = 0; $j < $season_html->count(); $j += 1) {
                $teaser = array();
                $teaser_html = $season_html->eq($j);

                $teaser['name'] = trim($teaser_html->filter('.title')->text(), "\n\"");
                try {
                    $teaser['url'] = 'www.allocine.fr'.$teaser_html->filter('a')->attr('href');
                    $teaser['id'] = intval(preg_replace('/\D/', '', substr($teaser['url'], 0, strpos($teaser['url'],'&cserie'))));
                } catch (Exception $e) {
                    $teaser['url'] = 'Unknown';
                    $teaser['id'] = -1;
                }
                $teaser['photo'] = $teaser_html->filter('img')->attr('src');

                $teasers[] = $teaser;
            }

            //$teasers['Season '.$i] = $season_teaser;
            $i += 1;
            $season_html = $crawler->filter('#seriesseasonnumber'.$i.' + section article');
        }

        return $teasers;
    }

    //Star
    //Filmographie http://www.allocine.fr/personne/fichepersonne-$id/filmographie/
    public static function scrappingStar($id) {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.allocine.fr/personne/fichepersonne_gen_cpersonne=' . $id . '.html');

        $star = [];

        $raw = $crawler->filter('#col_main');
        $list = $raw->filter(".list_item_p2v > li");

        $name = trim($raw->filter("#title > span")->text());
        $jobtitle = $list->eq(0)->filter("div")->text();
        $birthname = $list->eq(1)->filter("div")->text();
        $birthdate = $raw->filter("[itemprop='birthDate']")->text();
        $nationality = trim($list->eq(2)->filter("div")->text());
        $birthplace = trim(str_replace($birthdate, "", $list->eq(3)->filter("div")->text()), "\n()");
        $age = trim($list->eq(4)->filter("div")->text());
        $photo = $raw->filter("[itemprop='image']");

        $crawler = $client->request('GET', 'http://www.allocine.fr/personne/fichepersonne-' . $id . '/filmographie/');

        $rows = $crawler->filter("table > tbody > tr");
        $movies = [];
        for ($i = 0; $i < $rows->count(); $i++) {
            $row = $rows->eq($i);
            $movies[] = array(  "date" => trim($row->filter("td")->eq(0)->text()),
                                "title" => trim($row->filter("td")->eq(1)->text()),
                                "role" => trim($row->filter("td")->eq(2)->text()),
                            );
        }

        //$crawler = $client->request("GET", "http://www.allocine.fr/personne/fichepersonne-" . $id . "/biographie/");

        //$star["nationality"] = $nationality;
        //$star["jobtitle"] = $jobtitle;
        //$star["birthplace"] = $birthplace;
        //$star["birthname"] = $birthname;
        //$star["age"] = $age;

        $star["id"] = $id;
        $star["name"] = $name;
        $star["birth_date"] = $birthdate;
        $star["movies"] = $movies;
        $star["photo"] = $photo;
        $star["biography"] = "";

        return $star;
    }
}