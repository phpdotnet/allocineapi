<?php

class ActIn extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_person;

    /**
     *
     * @var integer
     */
    public $id_media;

    /**
     *
     * @var string
     */
    public $role;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_media', 'Media', 'id', array('alias' => 'Media'));
        $this->belongsTo('id_person', 'Person', 'id', array('alias' => 'Person'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Act_In';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ActIn[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ActIn
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
