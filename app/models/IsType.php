<?php

class IsType extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_media;

    /**
     *
     * @var string
     */
    public $type;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_media', 'Media', 'id', array('alias' => 'Media'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Is_Type';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return IsType[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return IsType
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
