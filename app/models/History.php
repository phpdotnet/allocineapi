<?php
/**
 * Created by PhpStorm.
 * User: F
 * Date: 15/06/2015
 * Time: 04:02
 */

class History extends \Phalcon\Mvc\Model {

    public $id;

    public $subscription_id;

    public $call_count;

    public $date;


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'History';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('subscription_id', 'Subscription', 'id', array('alias' => 'Subscription'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return IsType[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return IsType
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}