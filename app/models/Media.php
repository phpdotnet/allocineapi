<?php

class Media extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $synopsis;

    /**
     *
     * @var string
     */
    public $photo;

    /**
     *
     * @var double
     */
    public $press_score;

    /**
     *
     * @var double
     */
    public $viewer_score;

    /**
     *
     * @var string
     */
    public $date_out;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'ActIn', 'id_media', array('alias' => 'ActIn'));
        $this->hasMany('id', 'Film', 'id_media', array('alias' => 'Film'));
        $this->hasMany('id', 'IsType', 'id_media', array('alias' => 'IsType'));
        $this->hasMany('id', 'Production', 'id_media', array('alias' => 'Production'));
        $this->hasMany('id', 'Realisation', 'id_media', array('alias' => 'Realisation'));
        $this->hasMany('id', 'Serie', 'id_media', array('alias' => 'Serie'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Media';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Media[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Media
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
