<?php

class Realisation extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_media;

    /**
     *
     * @var integer
     */
    public $id_person;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_person', 'Person', 'id', array('alias' => 'Person'));
        $this->belongsTo('id_media', 'Media', 'id', array('alias' => 'Media'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Realisation';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Realisation[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Realisation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
