<?php

class Film extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_media;

    /**
     *
     * @var time
     */
    public $duration;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id_media', 'Screening', 'id_film', array('alias' => 'Screening'));
        $this->belongsTo('id_media', 'Media', 'id', array('alias' => 'Media'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Film';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Film[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Film
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
