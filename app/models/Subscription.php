<?php

class Subscription extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $start_date;

    /**
     *
     * @var string
     */
    public $key;

    /**
     *
     * @var integer
     */
    public $id_user;

    /**
     *
     * @var integer
     */
    public $id_offer;

    /**
     *
     * @var boolean
     */
    public $auto_renewal;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $ip_address;

    /**
     *
     * @var boolean
     */
    public $enable_out;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_user', 'User', 'id', array('alias' => 'User'));
        $this->belongsTo('id_offer', 'Offer', 'id', array('alias' => 'Offer'));

        $this->hasMany('id', 'History', 'subscription_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Subscription';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Subscription[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Subscription
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function isValid()
    {

    }

}
