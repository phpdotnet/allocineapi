<?php

class Person extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $biography;

    /**
     *
     * @var string
     */
    public $birth_date;

    /**
     *
     * @var string
     */
    public $photo;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'ActIn', 'id_person', array('alias' => 'ActIn'));
        $this->hasMany('id', 'Production', 'id_person', array('alias' => 'Production'));
        $this->hasMany('id', 'Realisation', 'id_person', array('alias' => 'Realisation'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Person';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Person[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Person
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
