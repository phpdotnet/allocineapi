<?php

class Season extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $id_serie;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $synopsis;

    /**
     *
     * @var string
     */
    public $date_out;

    /**
     *
     * @var integer
     */
    public $number;

    /**
     *
     * @var string
     */
    public $photo;

    /**
     *
     * @var double
     */
    public $viewer_score;

    /**
     *
     * @var double
     */
    public $press_score;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Episode', 'id_season', array('alias' => 'Episode'));
        $this->belongsTo('id_serie', 'Serie', 'id_media', array('alias' => 'Serie'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Season';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Season[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Season
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
