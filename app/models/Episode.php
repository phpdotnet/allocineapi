<?php

class Episode extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $synopsis;

    /**
     *
     * @var string
     */
    public $date_out;

    /**
     *
     * @var integer
     */
    public $number;

    /**
     *
     * @var string
     */
    public $photo;

    /**
     *
     * @var integer
     */
    public $id_season;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id_season', 'Season', 'id', array('alias' => 'Season'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Episode';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Episode[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Episode
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
