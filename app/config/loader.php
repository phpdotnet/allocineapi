<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->toolsDir,
        $config->application->libraryDir,
        $config->application->modelsDir,
        $config->application->viewsDir,
        $config->application->migrationsDir,
    )
)->register();

$loader->registerNamespaces(array(
    'App\Controllers' => __DIR__ . '/../controllers/',
    'App\Controllers\Api' => __DIR__ . '/../controllers/api/',
    'App\Controllers\Front' => __DIR__ . '/../controllers/front/',
    'Migrations' => __DIR__ . '/../migrations/',
    ))->register();

require_once __DIR__.'/../../vendor/autoload.php';