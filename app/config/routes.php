<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 6/1/15
 * Time: 1:58 PM
 */

use Phalcon\Mvc\Router;

// Create the router
$router = new Router();

$router->add(
    "/",
    array(
        "namespace"  => 'App\Controllers\Front',
        "controller" => 'index',
        "action"     => 'index'
    )
);

$router->add(
    "/wb/",
    array(
        "namespace"  => 'App\Controllers\Front',
        "controller" => 'index',
        "action"     => 'index'
    )
);

//Define routes
$router->add(
    "/wb/:controller/",
    array(
        "namespace"  => 'App\Controllers\Front',
        "controller" => 1,
        "action"     => 'index'
    )
);

$router->add(
    "/wb/:controller/:action/:params",
    array(
        "namespace"  => 'App\Controllers\Front',
        "controller" => 1,
        "action"     => 2,
        "params"     => 3,
    )
);

$router->add(
    "/api/:controller/:action/:params",
    array(
        "namespace"  => 'App\Controllers\Api',
        "controller" => 1,
        "action"     => 2,
        "params"     => 3,
    )
);