<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>AllocinéAPI</title>
    <meta name="generator" content="Mti" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    {{ stylesheet_link('css/bootstrap.min.css') }}
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    {{ javascript_include('js/bootstrap.min.js') }}

    {{ stylesheet_link('css/styles.css') }}

</head>
<body>
<header class="navbar navbar-bright navbar-fixed-top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">Home</a>
        </div>
        <nav class="collapse navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/"></a>
                </li>
            </ul>
        </nav>
    </div>
</header>

<div id="masthead">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <a href="/home">
                    <h1>AllocineAPI</h1>
                </a>
                <br>
            </div>

        </div>
    </div><!-- /cont -->

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-spacer"> </div>
            </div>
        </div>
    </div><!-- /cont -->

</div>


<div class="container">
    <div class="row">

        <div class="col-md-12">

            {{ content() }}

        </div><!--/col-12-->
    </div>
</div>

<hr>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <ul class="list-inline">
                    <li><i class="icon-facebook icon-2x"></i></li>
                    <li><i class="icon-twitter icon-2x"></i></li>
                    <li><i class="icon-google-plus icon-2x"></i></li>
                    <li><i class="icon-pinterest icon-2x"></i></li>
                </ul>

            </div>
            <div class="col-sm-6">
                <p class="pull-right">Built with <i class="icon-heart-empty"></i> from MTI</p>
            </div>
        </div>
    </div>
</footer>
<!-- script references -->

</body>
</html>