<h1>Welcome to AllocineAPI!</h1>
<form method="post" action="{{url('wb/account/register')}}" enctype="multipart/form-data">
	<div class="col-md-12">
		<div class="col-md-4">
			<h2>Register</h2>
			{% if not(error is empty) %}
				<div class="alert alert-danger" role="alert"><b>{{ error }}</b></div>
			{% endif %}
			<div class="form-group">
				<label>Username</label>
				{{ registerForm.render("username", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<label>Firstname</label>
				{{ registerForm.render("firstname", ["class": "form-control"]) }}
				<label>Lastname</label>
				{{ registerForm.render("lastname", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<label>Email</label>
				{{ registerForm.render("email", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<label>Password</label>
				{{ registerForm.render("password", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<label>Company</label>
				{{ registerForm.render("company", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<label>Photo</label>
				{{ registerForm.render("photo", ["class": "form-control"]) }}
			</div>
			<div class="form-group">
				<input class="btn btn-default" type="submit" value="Register">
			</div>
		</div>
	</div>	
</form>
<a href="index" class="btn btn-default">Back</a>