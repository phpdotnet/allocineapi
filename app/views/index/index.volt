{{ content() }}
{% if !connected %}
<!-- NOT CONNECTED -->
<div class="text-center">
    <h1>Welcome to AllocineAPI!</h1>
</div>
<div class="col-md-6 text-center">
    <p>
        This service enable you to extract various information on movies, series and celebrities from the Allociné website!
        <br>You can try it out for free right now!
    </p>

</div>
<div class="col-md-7 col-md-offset-7 text-center">
    <form method="post" action="{{url('wb/account/login')}}">
        <div class="col-md-12">
            <div class="col-md-4">
                <h2>Login</h2>
                {% if not(error is empty) %}
                <div class="alert alert-danger" role="alert"><b>{{ error }}</b></div>
                {% endif %}
				{% if not(success is empty) %}
				<div class="alert alert-success" role="alert"><b>{{ success }}</b></div>
				{% endif %}
                <div class="form-group">
                    <label>Username</label>
                    {{ loginForm.render("username", ["class": "form-control"]) }}
                </div>
                <div class="form-group">
                    <label>Password</label>
                    {{ loginForm.render("password", ["class": "form-control"]) }}
                </div>
                <div class="form-group">
                    <input class="btn btn-default" type="submit" value="Login"/>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="text-center">
    <a href="{{url('wb/index/register')}}">
        <button class="btn-blue">Register now!</button>
    </a>
</div>


{% else %}
<!-- CONNECTED -->
<h3>Mon compte</h3>
<div class="col-md-12">
	{% if photo %}
		<div class="col-md-3">
			<img src="data:image/jpeg;base64,{{ photo }}" width="128" />
		</div>
	{% endif %}
	<div class="col-md-3">
		<h4>{{ user.username }}</h4>
		<form method="get" action="{{url('wb/account/logout')}}">
			<input class="form-control" type="submit" value="logout">
		</form>
	</div>
</div>
<h3>Subscriptions</h3>
<ul>
{% for sub in subs %}
	<li class="col-md-4">
		<h3>{{sub.name}}</h3>
		<div>
			<b>Date</b>
			<p>{{ sub.start_date }} - {{ end_dates[sub.id] }} (Auto-Renewal: {{ sub.auto_renewal ? "ON" : "OFF" }})
				<form method="post" style="display:inline;" action="{{url('wb/profile/renewal')}}">
					<input hidden name="id" value="{{ sub.id }}">
					<input class="btn btn-default" type="submit" value="Toggle"/>
				</form>
			</p>
		</div>
		<div>
			<b>Key:</b>
			<p>{{ sub.key }}</p>
		</div>
		<div>
			<b>Offer:</b>
			<p>
				{{ sub.offer.name }} (Overuse: {{ sub.enable_out ? "ON" : "OFF" }})
				<form method="post" style="display:inline;" action="{{url('wb/profile/overage')}}">
					<input hidden name="id" value="{{ sub.id }}">
					<input class="btn btn-default" type="submit" value="Toggle"/>
				</form>
			</p>
		</div>
		<div>
			<b>Calls (Today)</b>
			<p>
				{% set today = 0 %}
				{% set total = 0 %}
					{% for history in sub.history %}
						{% set total += history.call_count %}
						{% if history.date == date('Y-m-d') %}
							{% set today = history.call_count %}
						{% endif %}
					{% endfor %}
				{{ today }} / {{ sub.offer.max_call }} ({{ (today * 100) / sub.offer.max_call }}%)
			</p>
		</div>
		<div>
			<b>Calls (Total)</b>
			<p>{{ total }} / {{ sub.offer.max_call * sub.offer.duration }} ({{ (total * 100) / (sub.offer.max_call * sub.offer.duration) }}%)</p>
		</div>
		<div>
			<b>Price (Overuse included)</b>
			<p>{{sub.offer.price + (today - sub.offer.max_call) > 0 ? (today - sub.offer.max_call) : 0 * 0.056}}$</p>
		</div>

		<form method="post" action="{{url('wb/profile/change')}}">
			<input hidden name="id" value="{{ sub.id }}">
			<div>
				<select class="form-control" name="id_offer">
					{% for offer in offers %}
						<option {% if offer.id == sub.id_offer %}disabled="true"{% endif %} 
						value="{{ offer.id }}">{{ offer.name }} - {{offer.max_call}} calls (${{offer.price}})</option>
					{% endfor %}
				</select>
				<input class="btn btn-default" type="submit" value="Change Offer">
			</div>
		</form>
		<form method="post" action="{{url('wb/profile/delete')}}">
			<input hidden name="id" value="{{ sub.id }}">
			<div>
				<input class="btn btn-default" type="submit" value="Delete Offer">
			</div>
		</form>
	</li>
{% endfor %}
	<li class="col-md-3">
		<h3>Add new application</h3>
		<form method="post" action="{{url('wb/profile/add')}}">
			<div class="form-group">
				<label>Application Name</label>
				<input class="form-control" name="name">
			</div>
			<div class="form-group">
				<label>Offer</label>
				<select class="form-control" name="id_offer">
					{% for offer in offers %}
					<option value="{{ offer.id }}">{{ offer.name }} - {{offer.max_call}} calls (${{offer.price}})</option>
					{% endfor %}
				</select>
			</div>
			<div class="form-group">
				<input type="checkbox" name="auto_renewal">Auto-Renewal
			</div>
			<div class="form-group">
				<input class="btn btn-default" type="submit" value="Add application">
			</div>
		</form>
	</li>
</ul>

{% endif %}