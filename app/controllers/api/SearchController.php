<?php

namespace App\Controllers\Api;

class SearchController extends BaseApiController
{

    public function indexAction($search)
    {
        echo \Scrapping::scrappingSearch($search);
    }

    public function allAction($search) {
        echo json_encode(\Scrapping::scrappingSearchAll($search));
    }

    public function filmAction($search) {
        echo json_encode(\Scrapping::scrappingSearchFilm($search));
    }

    public function serieAction($search) {
        echo json_encode(\Scrapping::scrappingSearchSerie($search));
    }

    public function starAction($search) {
        echo json_encode(\Scrapping::scrappingSearchCelibrity($search));
    }

    public function videoAction($search) {
        echo json_encode(\Scrapping::scrappingSearchVideo($search));
    }

    public function photoAction($search) {
        echo json_encode(\Scrapping::scrappingSearchPhoto($search));
    }
}

