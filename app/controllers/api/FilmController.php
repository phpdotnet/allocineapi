<?php

namespace App\Controllers\Api;
use Migrations\JobsManager;

/**
 * Class FilmController
 */
class FilmController extends \App\Controllers\Api\BaseApiController
{


    //Return a list in json of all films (or not)
    public function indexAction()
    {

    }

    // http://www.allocine.fr/recherche/11/?q=Avenger
    //Return a list of all films corresponding to the search
    public function searchAction($search)
    {
        echo json_encode(\Scrapping::scrappingSearchFilm($search));
    }

    public function getAction($id)
    {
        //Fiche Request to http://www.allocine.fr/film/fichefilm_gen_cfilm=$id.html
        //Casting Request to http://www.allocine.fr/film/fichefilm-$id/casting/
        //Bande Annonce Request to http://www.allocine.fr/video/cfilm=$id.html

        //Look in cache
        $redis = new \Predis\Client();
        if ($redis->exists($id)) {
            echo $redis->get($id);
        } else {
            try {
                $di = \Phalcon\DI\FactoryDefault::getDefault();
                $connection = $di['db'];
                $film = array();
                $statement = 'SELECT Film.*, Media.* FROM Film
                                  JOIN Media On Film.id_media = Media.id
                                  WHERE Film.id_media = :id;';
                $result = $connection->query($statement, array('id' => $id));

                if (!$result = $result->fetch()) {
                    throw new \UnexpectedValueException();
                }

                $film['id'] = $id;
                $film['duration'] = $result['duration'];
                $film['title'] = $result['name'];
                $film['photo'] = $result['photo'];
                $film['synopsys'] = $result['synopsis'];
                $film['date'] = $result['date_out'];
                $film['press_score'] = $result['press_score'];
                $film['viewer_score'] = $result['viewer_score'];

                $statement = 'SELECT * FROM Is_Type WHERE id_media = :id;';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $film['types'] = [];
                foreach ($result as $row)
                    $film['types'][] = $row['type'];

                $film['casting'] = array();
                $statement = 'SELECT Person.id as id, Person.name as name, Person.photo as photo FROM Realisation
                                                    JOIN Person ON Person.id = Realisation.id_person
                                                    WHERE Realisation.id_media = :id';
                $result = $connection->query($statement, array('id' => $id));
                if (!$result = $result->fetch())
                    throw new \UnexpectedValueException();

                $creator = array();
                $creator['id'] = $result['id'];
                $creator['name'] = $result['name'];
                $creator['photo'] = $result['photo'];
                $film['casting']['realisator'] = $creator;

                $statement = 'SELECT Person.id as id, Person.name as name, Person.photo as photo, Act_In.role as role FROM Person
                                                  JOIN Act_In ON Person.id = Act_In.id_person
                                                  WHERE Act_In.id_media = :id';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $film['casting']['actors'] = [];
                foreach ($result as $row) {
                    $actor = array();
                    $actor['id'] = $row['id'];
                    $actor['name'] = $row['name'];
                    $actor['photo'] = $row['photo'];
                    $actor['role'] = $row['role'];
                    $actor['url'] = 'http://www.allocine.fr/personne/fichepersonne_gen_cpersonne='.$actor['id'].'.html';

                    $film['casting']['actors'][] = $actor;
                }

                $statement = 'SELECT * FROM Teaser WHERE id_media= :id';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $film['teasers'] = [];

                foreach ($result as $row) {
                    $teaser = array();
                    $teaser['id'] = $row['id'];
                    $teaser['name'] = $row['name'];
                    $teaser['photo'] = $row['photo'];
                    $teaser['url'] = 'http://www.allocine.fr/video/player_gen_cmedia='.$teaser['id'].'&cfilm='.$id.'.html';
                    $film['teasers'][] = $teaser;
                }

                echo $film;
                $is_saved = true;
            } catch (\Exception $e) {
                //Scrapp allocine
                $film = \Scrapping::scrappingFilmFiche($id);
                $casting = \Scrapping::scrappingFilmCasting($id);
                $teasers = \Scrapping::scrappingFilmTeaser($id);
                $film['casting'] = $casting;
                $film['teasers'] = $teasers;
                $is_saved = false;
            }
            //Send the result to the client
            echo json_encode($film);

            //Save the result in cache
            $redis->set($id, json_encode($film));
            //Set for 2 days
            $redis->expire($id, 172800);

            //Save in database if needed
            if (!$is_saved) {
                //Put job
                JobsManager::putJob(array('film' => $id));
            }
        }
    }
}

