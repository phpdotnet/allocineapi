<?php
/**
 * Created by PhpStorm.
 * User: F
 * Date: 17/06/2015
 * Time: 19:06
 */

namespace App\Controllers\Api;

use Phalcon\Mvc\Controller;

class BaseApiController extends Controller {
    public function initialize() {
        $this->view->disable();
    }
}