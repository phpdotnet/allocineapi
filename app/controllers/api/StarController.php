<?php

namespace App\Controllers\Api;

class StarController extends BaseApiController
{
    //http://www.allocine.fr/recherche/5/?q=$search
    public function searchAction($search)
    {
        echo json_encode(\Scrapping::scrappingSearchCelibrity($search));
    }

    //Changez juste $id
    //Fiche http://www.allocine.fr/personne/fichepersonne_gen_cpersonne=$id.html
    //Filmographie http://www.allocine.fr/personne/fichepersonne-$id/filmographie/
    public function indexAction()
    {
    }

    public function getAction($id) {
        //Look in cache
        $redis = new \Predis\Client();
        if ($redis->exists($id)) {
            echo $redis->get($id);
        } else {
            try {
                //Look in DataBase


                $di = \Phalcon\DI\FactoryDefault::getDefault();
                $connection = $di['db'];
                $star = array();
                $statement = 'SELECT Person.* FROM Person WHERE Person.id = :id;';
                $result = $connection->query($statement, array('id' => $id));

                if (!$result = $result->fetch()) {
                    throw new \UnexpectedValueException();
                }

                $star['id'] = $id;
                $star['name'] = $result['name'];
                $star['biography'] = $result['biography'];
                $star['birth_date'] = $result['birth_date'];
                $star['photo'] = $result['photo'];

                $star['films'] = array();
                $statement = 'SELECT Media.id as id, Media.name as name, Media.synopsis as synopsis, Media.photo as photo,
                Media.press_score as press_score, Media.viewer_score as viewer_score, Media.date_out as date_out FROM Media
                            JOIN Act_In ON Act_In.id_media = Media.id
                            JOIN Production ON Production.id_media = Media.id
                            JOIN Realisation ON Realisation.id_media = Media.id
                            WHERE Act_In.id_person = :id;';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                foreach ($result as $row) {
                    $film = array();
                    $film['id'] = $row['id'];
                    $film['name'] = $row['name'];
                    $film['photo'] = $row['photo'];
                    $film['synopsis'] = $row['synopsis'];
                    $film['press_score'] = $row['press_score'];
                    $film['viewer_score'] = $row['viewer_score'];
                    $film['date_out'] = $row['date_out'];
                    //$film['role'] = $row['role'];
                    $film['url'] = 'http://www.allocine.fr/film/fichefilm_gen_cfilm='.$film['id'].'.html';

                    $star["films"][] = $film;
                }

                $is_saved = true;
            } catch (Exception $e) {
                //Scrapp allocine
                $star = \Scrapping::scrappingStar($id);
                $is_saved = false;
            }
            //Send the result to the client
            echo json_encode($star);

            //Save the result in cache
            $redis->set($id, json_encode($star));
            //Set for 2 days
            $redis->expire($id, 172800);

            //Save in database if needed
            if (!$is_saved) {
                //Put job
                JobsManager::putJob(array('star' => $id));
            }
        }
    }

}

