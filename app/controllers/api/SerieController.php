<?php

namespace App\Controllers\Api;

use Goutte\Client;
use Migrations\JobsManager;
use Predis\ClientException;

class SerieController extends BaseApiController
{

    //http://www.allocine.fr/recherche/6/?q=search
    public function searchAction($search)
    {
        echo json_encode(\Scrapping::scrappingSearchSerie($search));
    }

    //Fiche http://www.allocine.fr/series/ficheserie_gen_cserie=$id.html
    //Casting http://www.allocine.fr/series/ficheserie-$id/casting/
    //Saison http://www.allocine.fr/series/ficheserie-$id/saison-$idseason/
    public function indexAction()
    {
    }


    public function getAction($id)
    {
        //Look in cache
        $redis = new \Predis\Client();
        if ($redis->exists($id)) {
            echo $redis->get($id);
        } else {
            try {
                //Look in DataBase
                $di = \Phalcon\DI\FactoryDefault::getDefault();
                $connection = $di['db'];
                $serie = array();
                $statement = 'SELECT Serie.*, Media.* FROM Serie
                                  JOIN Media On Serie.id_media = Media.id
                                  WHERE Serie.id_media = :id;';
                $result = $connection->query($statement, array('id' => $id));

                if (!$result = $result->fetch()) {
                    throw new \UnexpectedValueException();
                }

                $serie['id'] = $id;
                $serie['format'] = $result['format'];
                $serie['title'] = $result['name'];
                $serie['photo'] = $result['photo'];
                $serie['synopsys'] = $result['synopsis'];
                $serie['date'] = $result['date_out'];
                $serie['press_score'] = $result['press_score'];
                $serie['viewer_score'] = $result['viewer_score'];

                $statement = 'SELECT * FROM Is_Type WHERE id_media = :id;';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $serie['types'] = [];
                foreach ($result as $row)
                    $serie['types'][] = $row['type'];

                $serie['casting'] = array();
                $statement = 'SELECT Person.id as id, Person.name as name, Person.photo as photo FROM Realisation
                                                    JOIN Person ON Person.id = Realisation.id_person
                                                    WHERE Realisation.id_media = :id';
                $result = $connection->query($statement, array('id' => $id));
                if (!$result = $result->fetch())
                     throw new \UnexpectedValueException();

                $creator = array();
                $creator['id'] = $result['id'];
                $creator['name'] = $result['name'];
                $creator['photo'] = $result['photo'];
                $serie['casting']['creator'] = $creator;

                $statement = 'SELECT Person.id as id, Person.name as name, Person.photo as photo, Act_In.role as role FROM Person
                                                  JOIN Act_In ON Person.id = Act_In.id_person
                                                  WHERE Act_In.id_media = :id';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $serie['casting']['actors'] = [];
                foreach ($result as $row) {
                    $actor = array();
                    $actor['id'] = $row['id'];
                    $actor['name'] = $row['name'];
                    $actor['photo'] = $row['photo'];
                    $actor['role'] = $row['role'];
                    $actor['url'] = 'http://www.allocine.fr/personne/fichepersonne_gen_cpersonne='.$actor['id'].'.html';

                    $serie['casting']['actors'][] = $actor;
                }

                $statement = 'SELECT * FROM Teaser WHERE id_media= :id';
                $result = $connection->query($statement, array('id' => $id));
                $result = $result->fetchAll();
                $serie['teasers'] = [];

                foreach ($result as $row) {
                    $teaser = array();
                    $teaser['id'] = $row['id'];
                    $teaser['name'] = $row['name'];
                    $teaser['photo'] = $row['photo'];
                    $teaser['url'] = 'http://www.allocine.fr/video/player_gen_cmedia='.$teaser['id'].'&cserie='.$id.'.html';
                    $serie['teasers'][] = $teaser;
                }

                if (!$serie)
                    throw new \UnexpectedValueException();
                $is_saved = true;
            } catch (\Exception $e) {
                //Scrapp allocine
                $serie = \Scrapping::scrappingSerieFiche($id);
                //$casting = \Scrapping::scrappingSerieCasting($id);
                $teasers = \Scrapping::scrappingSerieTeaser($id);
                //$serie['casting'] = $casting;
                $serie['teasers'] = $teasers;
                $is_saved = false;
            }
            //Send the result to the client
            echo json_encode($serie);

            //Save the result in cache
            $redis->set($id, json_encode($serie));
            //Set for 2 days
            $redis->expire($id, 172800);

            //Save in database if needed
            if (!$is_saved) {
                //Put job
                JobsManager::putJob(array('serie' => $id));
            }
        }
    }
}

