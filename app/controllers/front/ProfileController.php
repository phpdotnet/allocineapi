<?php

namespace App\Controllers\Front;

class ProfileController extends ControllerBase {
	public function indexAction() {

	}

    public function addAction() {
        if ($this->request->isPost()) {
            $sub = new \Subscription();
            $sub->start_date = date('Y-m-d');
            
            $sub->id_offer = $this->request->get("id_offer");
            $sub->key = \UuidGenerator::v4();
            $sub->name = htmlspecialchars($this->request->get("name"));
            $sub->id_user = $this->session->get("userid");
            $sub->auto_renewal = $this->request->get("auto_renewal") ? "1" : "0";
            $sub->ip_address = $this->request->getClientAddress();
            $sub->enable_out = "0";

            if ($sub->create()) {
            } else {
                $messages = [];
                foreach ($sub->getMessages() as $msg) {
                    $messages[] = (string)$msg;
                }
                $this->session->set("error", $messages);
            }
        } else {
            $this->session->set("error", "Bad request");
        }
        $this->response->redirect("wb/index/index");
    }

    public function changeAction() {
        if ($this->request->isPost()) {
            $sub_id = $this->request->get("id");

            $sub = \Subscription::findFirst("id='$sub_id'");
            if ($sub && $this->session->get("userid") == $sub->id_user) {
                $sub->id_offer = $this->request->get("id_offer");
                $sub->start_date = date('Y-m-d');
                $sub->save();
            } else {
                $this->session->set("error", "Unauthorized");
            }

        } else {
            $this->session->set("error", "Bad request");
        }
        $this->response->redirect("wb/index/index");
    }

    public function offersAction()
    {
        $offers = \Offer::find(array(
            "order" => "price",
            "limit" => 3
        ));

        if (!$offers || !isset($offers))
            $offers = array();

        $this->view->setVar("offers", $offers);
    }

    public function deleteAction() {
        if ($this->request->isPost()) {
            $sub_id = $this->request->get("id");
            $sub = \Subscription::findFirst("id='$sub_id'");
            if ($sub && $this->session->get("userid") == $sub->id_user) {
                if ($sub->delete()) {
                } else {
                    $this->session->set("error", "Error while deleting");
                }
            } else {
            	$this->session->set("error", "Unauthorized");
            }
        }
        $this->response->redirect("/wb/index/index");
    }

    public function renewalAction() {
        if ($this->request->isPost()) {
            $sub_id = $this->request->get("id");
            $sub = \Subscription::findFirst("id='$sub_id'");
            if ($sub && $this->session->get("userid") == $sub->id_user) {
                $sub->auto_renewal = $sub->auto_renewal ? "0" : "1";
                $sub->save();
            } else {
            	$this->session->set("error", "Unauthorized");
            }
        }
        $this->response->redirect("/wb/index/index");
    }

    public function overageAction() {
        if ($this->request->isPost()) {
            $sub_id = $this->request->get("id");
            $sub = \Subscription::findFirst("id='$sub_id'");
            if ($sub && $this->session->get("userid") == $sub->id_user) {
                $sub->enable_out = $sub->enable_out ? "0" : "1";
                $sub->save();
            } else {
            	$this->session->set("error", "Unauthorized");
            }
        }
        $this->response->redirect("/wb/index/index");
    }
}