<?php

namespace App\Controllers\Front;

class AccountController extends ControllerBase {
	public function indexAction() {

	}

    public function loginAction()
    {        
        if ($this->request->isPost()) {
            $name = htmlspecialchars($this->request->getPost("username"));
            $pass = htmlspecialchars($this->request->getPost("password"));

            $user = \User::findFirst(array("username='$name' AND password='" . md5($pass) . "'"));
            if ($user) {
                $this->session->set("userid", $user->id);
            } else {
                $this->session->set("error", "Wrong credentials");
            }
        } else {
            $this->session->set("error", "Bad request");
        }
        $this->response->redirect("/wb/index/index");
    }

    public function logoutAction() {
        if ($this->session->has("userid")) {
            $this->session->destroy();
            $this->session->set("error", "Logout successfull");
            $this->response->redirect("/wb/index/index");
        } else {
            $this->session->set("error", "You must be logged");
        }
        $this->response->redirect("/wb/index/index");
    }

    public function registerAction() {
        if ($this->request->isPost() == true) {
            $user = new \User();
            $user->username = htmlspecialchars($this->request->getPost("username"));
            $user->password = md5($this->request->getPost("password"));
            $user->email = htmlspecialchars($this->request->getPost("email"));
            $user->firstname = htmlspecialchars($this->request->getPost("firstname"));
            $user->lastname = htmlspecialchars($this->request->getPost("lastname"));
            $user->company = htmlspecialchars($this->request->getPost("company"));
            $user->photo = "";
            if ($this->request->hasFiles()) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $user->photo = file_get_contents($file->getTempName());
                }
            }
            $user->ip_address = $this->request->getClientAddress();

            $temp = \User::findFirst(array("username='" . $user->username . "'"));

            if (!$temp) {
                if ($user->create()) {
                    $this->session->set("success", "Account registered");
                    $this->response->redirect("/wb/index/index");
                } else {
                    $messages = '';
                    foreach ($user->getMessages() as $msg) {
                        $messages .= (string)$msg . ", ";
                    }
                    $this->session->set("error", $messages);
                    $this->response->redirect("/wb/index/register");
                }
            } else {
                $this->session->set("error", "User already exists");
                $this->response->redirect("/wb/index/register");
            }
        }
    }
}