<?php

namespace App\Controllers\Front;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\File;

use Phalcon\Http\Request;
use Phalcon\Http\Response;


class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->error = "";
        if ($this->session->has("error")) {
            $this->view->error = $this->session->get("error");
            $this->session->remove("error");
        }
        $this->view->success = "";
        if ($this->session->has("success")) {
            $this->view->success = $this->session->get("success");
            $this->session->remove("success");
        }

        $this->view->connected = false;
        if ($this->session->has("userid")) {
            $userid = $this->session->get("userid");
            $user = \User::findFirst(array("id='" . $userid . "'"));
            if ($user) {
                $this->view->connected = true;                
                $this->view->subs = \Subscription::find(array("id_user='" . $user->id . "'"));

                $end_dates = [];
                foreach ($this->view->subs as $sub) {
                    $end_dates[$sub->id] = date('Y-m-d', strtotime($sub->start_date . " + " . $sub->offer->duration . " days"));
                }
                $this->view->end_dates = $end_dates;

                $this->view->offers = \Offer::find();
                $this->view->user = $user;

                $this->view->photo = false;
                if ($user->photo) {
                    $this->view->photo = base64_encode($user->photo);
                }
            }
        }
        
        $loginForm = new Form();

        $loginForm->add(new Text("username"));
        $loginForm->add(new Password("password"));

        $this->view->loginForm = $loginForm;
    }

    public function registerAction() {
        $this->view->error = "";
        if ($this->session->has("error")) {
            $this->view->error = $this->session->get("error");
            $this->session->remove("error");
        }
        
        $registerForm = new Form();
        $registerForm->add(new Text("username"));
        $registerForm->add(new Password("password"));
        $registerForm->add(new Text("firstname"));
        $registerForm->add(new Text("lastname"));
        $registerForm->add(new Text("email"));
        $registerForm->add(new Text("company"));
        $registerForm->add(new File("photo"));

        $this->view->registerForm = $registerForm;
    }

    public function show401Action()
    {
        $this->view->disable();
        echo json_encode(array("error" =>'Error 401'));
    }

    public function showMaxCallsAction()
    {
        $this->view->disable();
        echo json_encode(array("error" => 'Calls exceeding the daily given quota!'));
    }
}