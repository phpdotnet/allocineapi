<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Mvc\Dispatcher;

class Security extends Plugin
{
    public function getAcl()
    {
        if (!isset($this->persistent->acl)) {

            $acl = new AclList();
            $acl->setDefaultAction(Acl::DENY);

            $roles = array(
                'users'  => new Role('Users'),
                'anonymous' => new Role('Anonymous')
            );

            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            $consumableResources = array(
                'film'    => array('index', 'search', 'get'),
                'search'  => array('index', 'all', 'film', 'serie', 'star', 'video', 'photo'),
                'serie' => array('search', 'index', 'get'),
                'star'  => array('search', 'index', 'get')
            );
            foreach ($consumableResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            $privateResources = array(
            );
            foreach ($privateResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            $publicResources = array(
                'index'      => array('index', 'register'),
                'account'       => array('index', 'login', 'logout'),
                'register'      => array('index'),
                'profile'      => array('index'),
            );
            foreach ($publicResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            foreach ($roles as $role) {
                foreach ($publicResources as $resource => $actions) {
                    foreach ($actions as $action){
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            foreach ($privateResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('Users', $resource, $action);
                }
            }

            $acl->addRole(new Role('Consumers'));

            foreach ($consumableResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('Consumers', $resource, $action);
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
        }
        return $this->persistent->acl;
    }

    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
        $role = 'Anonymous';

        $over_max_call = false;

        if ($dispatcher->getNamespaceName() === 'App\Controllers\Api') {
            $token = $this->request->getQuery('token', 'string');

            $sub = Subscription::findFirst(array(
                "conditions" => "key = ?1",
                "bind"       => array(1 => $token)
            ));

            if ($sub)
            {
                $role = 'Consumers';

                $hist = History::findFirst(array(
                    "conditions" => "subscription_id = :id: and date = :date:",
                    "bind" => array("id" => $sub->id, "date" => date('Y-m-d'))
                ));

                if ($hist) {
                    if ($hist->call_count >= $sub->offer->max_call && $sub->enable_out == false)
                        $over_max_call = true;
                }
            }
        }

        if ($dispatcher->getNamespaceName() === 'App\Controllers\Front') {
            if ($auth)
                $role = 'Users';
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);


        if ($allowed != Acl::ALLOW) {
            if ($dispatcher->getControllerName() != 'index') {

                $dispatcher->forward(array(
                    'namespace'  => 'App\Controllers\Front',
                    'controller' => 'index',
                    'action'     => 'show401'
                ));

                return false;
            }
            return;
        }

        if ($over_max_call) {
            if ($dispatcher->getControllerName() != 'index') {

                $dispatcher->forward(array(
                    'namespace'  => 'App\Controllers\Front',
                    'controller' => 'index',
                    'action'     => 'showMaxCalls'
                ));

                return false;
            }
            return;
        }
    }

    public function afterDispatch(Event $event, Dispatcher $dispatcher)
    {
        if ($dispatcher->getNamespaceName() === 'App\Controllers\Api')
        {
            $token = $this->request->getQuery('token', 'string');

            $sub = Subscription::findFirst(array(
                "conditions" => "key = ?1",
                "bind"       => array(1 => $token)
            ));

            $hist = History::findFirst(array(
                "conditions" => "subscription_id = :id: and date = :date:",
                "bind" => array("id" => $sub->id, "date" => date('Y-m-d'))
            ));

            if (!$hist) {
                $hist = new History();

                $hist->subscription_id = $sub->id;
                $hist->call_count = 0;
                $hist->date = date('Y-m-d');
            }

            $hist->call_count++;

            if ($hist->save() == false) {
                if ($dispatcher->getControllerName() != 'index') {

                    $dispatcher->forward(array(
                        'namespace'  => 'App\Controllers\Front',
                        'controller' => 'index',
                        'action'     => 'show401'
                    ));

                    return false;
                }
                return;
            }

        }
    }

}