<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 6/15/15
 * Time: 12:05 PM
 */

namespace Migrations;

use Phalcon\Queue\Beanstalk;

class JobsManager {
    private static $queue = null;
    private static $host = '0.0.0.0';
    private static $nb_max_thread = 3;
    public static $nb_thread = 0;

    private static function createQueue() {
        $queue = new Beanstalk(array('host' => JobsManager::$host));
        $queue->connect();
        return $queue;
    }

    public static function getQueue() {
        if (is_null(self::$queue))
            self::$queue = JobsManager::createQueue();

        return self::$queue;
    }

    public static function putJob($job) {
        //Put the job
        self::getQueue()->put($job);
    }
}