-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2015 at 01:23 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `AllocineAPIDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `Act_In`
--

CREATE TABLE IF NOT EXISTS `Act_In` (
  `id_person` bigint(20) unsigned NOT NULL,
  `id_media` bigint(20) unsigned NOT NULL,
  `role` varchar(150) NOT NULL,
  KEY `id_person` (`id_person`,`id_media`),
  KEY `id_media` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Episode`
--

CREATE TABLE IF NOT EXISTS `Episode` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `synopsis` text NOT NULL,
  `date_out` date NOT NULL,
  `number` tinyint(4) NOT NULL,
  `photo` blob NOT NULL,
  `id_season` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_season` (`id_season`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Film`
--

CREATE TABLE IF NOT EXISTS `Film` (
  `id_media` bigint(20) unsigned NOT NULL,
  `duration` time DEFAULT NULL,
  PRIMARY KEY (`id_media`),
  UNIQUE KEY `id_media` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Film`
--

INSERT INTO `Film` (`id_media`, `duration`) VALUES
(125943, NULL),
(128600, '00:00:02'),
(141609, '00:00:01'),
(141808, '00:00:02'),
(200399, '00:00:01'),
(202807, '00:00:01'),
(204925, NULL),
(205806, '00:00:02'),
(211042, NULL),
(211997, '00:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `History`
--

CREATE TABLE IF NOT EXISTS `History` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` bigint(20) unsigned DEFAULT NULL,
  `call_count` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `subscription_id` (`subscription_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `History`
--

INSERT INTO `History` (`id`, `subscription_id`, `call_count`, `date`) VALUES
(1, 2, 32, '2015-06-15'),
(2, NULL, 1, '2015-06-15'),
(3, 2, 1, '2015-06-16'),
(4, 2, 15, '2015-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `Is_Type`
--

CREATE TABLE IF NOT EXISTS `Is_Type` (
  `id_media` bigint(20) unsigned NOT NULL,
  `type` varchar(50) NOT NULL,
  KEY `id_media` (`id_media`,`type`),
  KEY `id_type` (`type`),
  KEY `id_media_2` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Is_Type`
--

INSERT INTO `Is_Type` (`id_media`, `type`) VALUES
(125943, 'Thriller'),
(128600, 'Drame'),
(141609, 'Action'),
(141609, 'Science fiction'),
(141808, 'Drame'),
(141808, 'Romance'),
(200399, 'Action'),
(200399, 'Drame'),
(200399, 'Western'),
(202807, 'ComÃ©die dramatique'),
(204925, 'Aventure'),
(204925, 'Drame'),
(204925, 'Science fiction'),
(205806, 'Action'),
(211042, 'Drame'),
(211042, 'Thriller'),
(211997, 'Thriller');

-- --------------------------------------------------------

--
-- Table structure for table `Media`
--

CREATE TABLE IF NOT EXISTS `Media` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `synopsis` text NOT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `press_score` float DEFAULT '0',
  `viewer_score` float DEFAULT '0',
  `date_out` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Media`
--

INSERT INTO `Media` (`id`, `name`, `synopsis`, `photo`, `press_score`, `viewer_score`, `date_out`) VALUES
(125943, 'Under Still Waters', 'Un couple de jeunes mariÃ©s fait la rencontre d''un Ã©tranger qui vient menacer leur mariage et leur vie...', 'http://fr.web.img4.acsta.net/r_160_240/b_1_d6d6d6/commons/emptymedia/empty_photo.jpg', 0, 0, NULL),
(128600, 'Wall Street : l''argent ne dort jamais', 'Wall Street, New York : en plein krach boursier de 2008, un jeune trader, Jacob Moore, est prÃªt Ã  tout pour venger son mentor, que d''obscures tractations financiÃ¨res ont poussÃ© au suicide. Il demande de l''aide Ã  Gordon Gekko, le meilleur - et le pire - des gourous de la finance, qui vient de sortir de 20 ans de prison pour dÃ©lit d''initiÃ©. Jacob va apprendre Ã  ses dÃ©pens que Gekko reste un maÃ®tre de la manipulation, et que l''argent ne dort jamais.', 'http://fr.web.img2.acsta.net/r_160_240/b_1_d6d6d6/medias/nmedia/18/74/10/97/19489291.jpg', 2.3, 2.6, '0000-00-00'),
(141609, 'Terminator Genisys', '', 'http://fr.web.img2.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/05/27/10/44/236893.jpg', NULL, NULL, '0000-00-00'),
(141808, 'Gatsby le Magnifique', 'Printemps 1922. L''Ã©poque est propice au relÃ¢chement des mÅ“urs, Ã  l''essor du jazz et Ã  l''enrichissement des contrebandiers d''alcoolâ€¦ Apprenti Ã©crivain, Nick Carraway quitte la rÃ©gion du Middle-West pour s''installer Ã  New York. Voulant sa part du rÃªve amÃ©ricain, il vit dÃ©sormais entourÃ© d''un mystÃ©rieux millionnaire, Jay Gatsby, qui s''Ã©tourdit en fÃªtes mondaines, et de sa cousine Daisy et de son mari volage, Tom Buchanan, issu de sang noble. C''est ainsi que Nick se retrouve au cÅ“ur du monde fascinant des milliardaires, de leurs illusions, de leurs amours et de leurs mensonges. TÃ©moin privilÃ©giÃ© de son temps, il se met Ã  Ã©crire une histoire oÃ¹ se mÃªlent des amours impossibles, des rÃªves d''absolu et des tragÃ©dies ravageuses et, chemin faisant, nous tend un miroir oÃ¹ se reflÃ¨tent notre Ã©poque moderne et ses combats.', 'http://fr.web.img4.acsta.net/r_160_240/b_1_d6d6d6/medias/nmedia/18/86/89/36/20531934.jpg', 2.8, 4, '0000-00-00'),
(194009, 'Le monde de Nathan', 'Nathan est un adolescent souffrant de troubles autistiques et prodige en mathématiques. Brillant mais asocial, il fuit toute manifestation d’affection, même venant de sa mère. Il tisse pourtant une amitié étonnante avec son professeur anticonformiste Mr. Humphreys, qui le pousse à intégrer l’équipe britannique et participer aux prochaines Olympiades Internationales de Mathématiques. De la banlieue anglaise à Cambridge en passant par Taipei, la vie de Nathan pourrait bien prendre un tour nouveau…', 'http://fr.web.img6.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/04/30/14/50/452714.jpg', 3.1, 3.6, '0000-00-00'),
(200399, 'Des hommes sans loi', '1931. Au cÅ“ur de lâ€™AmÃ©rique en pleine Prohibition, dans le comtÃ© de Franklin en Virginie, Ã©tat cÃ©lÃ¨bre pour sa production dâ€™alcool de contrebande, les trois frÃ¨res Bondurant sont des trafiquants notoires : Jack, le plus jeune, ambitieux et impulsif, veut transformer la petite affaire familiale en trafic dâ€™envergure. Il rÃªve de beaux costumes, dâ€™armes, et espÃ¨re impressionner la sublime Berthaâ€¦ Howard, le cadet, est le bagarreur de la famille. Loyal, son bon sens se dissout rÃ©guliÃ¨rement dans lâ€™alcool quâ€™il ne sait pas refuserâ€¦ Forrest, lâ€™aÃ®nÃ©, fait figure de chef et reste dÃ©terminÃ© Ã  protÃ©ger sa famille des nouvelles rÃ¨gles quâ€™impose un nouveau monde Ã©conomique. Lorsque Maggie dÃ©barque fuyant Chicago, il la prend aussi sous sa protection. Seuls contre une police corrompue, une justice arbitraire et des gangsters rivaux, les trois frÃ¨res Ã©crivent leur lÃ©gende : une lutte pour rester sur leur propre chemin, au cours de la premiÃ¨re grande ruÃ©e vers lâ€™or du crime.', 'http://fr.web.img4.acsta.net/r_160_240/b_1_d6d6d6/medias/nmedia/18/90/46/07/20182376.jpg', 3.4, 4.1, '0000-00-00'),
(202807, 'Nos futurs', 'Deux amis dâ€™enfance, qui sâ€™Ã©taient perdus de vue depuis le lycÃ©e, se retrouvent et partent en quÃªte de leurs souvenirsâ€¦', 'http://fr.web.img1.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/06/12/12/17/424614.jpg', NULL, NULL, '0000-00-00'),
(204925, 'Hunger Games â€“ La RÃ©volte : Partie 2', 'Alors que Panem est ravagÃ© par une guerre dÃ©sormais totale, Katniss et le PrÃ©sident Snow vont sâ€™affronter pour la derniÃ¨re fois. Katniss et ses plus proches amis â€“ Gale, Finnick, et Peeta â€“ sont envoyÃ©s en mission pour le District 13 : ils vont risquer leur vie pour tenter dâ€™assassiner le PrÃ©sident Snow, qui sâ€™est jurÃ© de dÃ©truire Katniss. Les piÃ¨ges mortels, les ennemis et les choix dÃ©chirants qui attendent Katniss seront des Ã©preuves bien pires que tout ce quâ€™elle a dÃ©jÃ  pu affronter dans lâ€™arÃ¨neâ€¦', 'http://fr.web.img3.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/03/19/16/58/132730.jpg', NULL, NULL, NULL),
(205806, 'White House Down', '', 'http://fr.web.img6.acsta.net/r_160_240/b_1_d6d6d6/pictures/210/175/21017526_20130731153915065.jpg', 2.9, 3.4, '0000-00-00'),
(211042, 'Swerve', '', 'http://fr.web.img5.acsta.net/r_160_240/b_1_d6d6d6/medias/nmedia/18/92/98/27/20306941.jpg', 0, 3.1, NULL),
(211997, 'Enfant 44', 'Hiver 1952, Moscou. Leo Demidov est un brillant agent de la police secrÃ¨te soviÃ©tique, promis Ã  un grand avenir au sein du Parti. Lorsque le corps dâ€™un enfant est retrouvÃ© sur une voie ferrÃ©e, il est chargÃ© de classer lâ€™affaire. Il sâ€™agit dâ€™un accident, Staline ayant dÃ©crÃ©tÃ© que le crime ne pouvait exister dans le parfait Etat communiste. Mais peu Ã  peu, le doute sâ€™installe dans lâ€™esprit de LÃ©o et il dÃ©couvre que dâ€™autres enfants ont Ã©tÃ© victimes Â« dâ€™accidents Â» similaires. TombÃ© en disgrÃ¢ce, soupÃ§onnÃ© de trahison, LÃ©o est contraint Ã  l''exil avec sa femme, RaÃ¯ssa. Prenant tous les risques, LÃ©o et RaÃ¯ssa vont se lancer dans la traque de ce tueur en sÃ©rie invisible, qui fera d''eux des ennemis du peuple...', 'http://fr.web.img6.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/03/13/18/31/574925.jpg', 2.4, 3.4, '0000-00-00'),
(229877, 'Un Français', 'Avec ses copains, Braguette, Grand-Guy, Marvin, Marco cogne les Arabes et colle les affiches de l''extrême droite. Jusqu''au moment où il sent que, malgré lui, toute cette haine l''abandonne. Mais comment se débarrasser de la violence, de la colère, de la bêtise qu''on a en soi ? C''est le parcours d''un salaud qui va tenter de devenir quelqu''un de bien.', 'http://fr.web.img1.acsta.net/r_160_240/b_1_d6d6d6/pictures/15/04/22/16/06/068738.jpg', 3, 2.8, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `Offer`
--

CREATE TABLE IF NOT EXISTS `Offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `max_call` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Offer`
--

INSERT INTO `Offer` (`id`, `name`, `max_call`, `duration`, `price`) VALUES
(1, 'Free', 50, 30, 0), (2, 'Starter Pack', 100, 30, 5), (3, 'Small organization', 200, 30, 8),
  (4, 'Data extractor', 600, 30, 12), (5, 'Admin demo', -1, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Person`
--

CREATE TABLE IF NOT EXISTS `Person` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `biography` text NOT NULL,
  `birth_date` date DEFAULT NULL,
  `photo` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Production`
--

CREATE TABLE IF NOT EXISTS `Production` (
  `id_media` bigint(20) unsigned NOT NULL,
  `id_person` bigint(20) unsigned NOT NULL,
  KEY `id_media` (`id_media`,`id_person`),
  KEY `id_person` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Realisation`
--

CREATE TABLE IF NOT EXISTS `Realisation` (
  `id_media` bigint(20) unsigned NOT NULL,
  `id_person` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id_media_2` (`id_media`),
  KEY `id_media` (`id_media`,`id_person`),
  KEY `id_person` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Screening`
--

CREATE TABLE IF NOT EXISTS `Screening` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `audio` enum('VF','VO') NOT NULL,
  `screen_type` enum('numeric','analogic','3D','imax') NOT NULL,
  `id_film` bigint(20) unsigned NOT NULL,
  `id_theater` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_film` (`id_film`),
  KEY `id_theater` (`id_theater`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Season`
--

CREATE TABLE IF NOT EXISTS `Season` (
  `id` bigint(20) unsigned NOT NULL,
  `id_serie` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `synopsis` text NOT NULL,
  `date_out` date NOT NULL,
  `number` tinyint(4) NOT NULL,
  `photo` blob NOT NULL,
  `viewer_score` float NOT NULL,
  `press_score` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_serie` (`id_serie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Serie`
--

CREATE TABLE IF NOT EXISTS `Serie` (
  `id_media` bigint(20) unsigned NOT NULL,
  `format` time NOT NULL,
  PRIMARY KEY (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Subscription`
--

CREATE TABLE IF NOT EXISTS `Subscription` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `key` varchar(100) NOT NULL,
  `id_user` bigint(20) unsigned NOT NULL,
  `id_offer` int(11) NOT NULL,
  `auto_renewal` boolean NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `enable_out` boolean NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_offer` (`id_offer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Subscription`
--

INSERT INTO `Subscription` (`id`, `start_date`, `key`, `id_user`, `id_offer`, `auto_renewal`, `ip_address`, `name`, `enable_out`) VALUES
(0, '2015-06-17', 'f699eb79-0b24-4b5d-89e7-6763fa04bd4a', 0, 4, '0', '127.0.0.1', 'TestAdmin', '0');
(1, '2015-06-17', '73a45ef9-f8b7-4a65-9d1f-fa9fd6f3c95e', 0, 0, '0', '127.0.0.1', 'AppliClassique', '0');
(2, '2015-06-17', 'd55d7e0e-7b24-4fb8-9302-aa60b92ddc0d', 0, 0, '0', '127.0.0.1', 'AppliHorsForfait', '1');
(3, '2015-06-15', 'abc', 2, 1, '0', '', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `Teaser`
--

CREATE TABLE IF NOT EXISTS `Teaser` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `url` varchar(300) NOT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `id_media` bigint(11) unsigned NOT NULL,
  KEY `id_media` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Theater`
--

CREATE TABLE IF NOT EXISTS `Theater` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `photo` blob,
  `ip_address` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `username`, `firstname`, `lastname`, `company`, `email`, `password`, `photo`, `ip_address`) VALUES
(2, 'Vince', 'Vincent', 'Molinié', 'EPITA', 'vincent.molinie@epita.fr', 'b4d3d15b35c2b26f00cb84113d7ce87b4fc20e91659e9d39e2f8dd86944a508f', NULL, '82.227.10.100'),
  (3, 'root', 'root', 'root', 'EPITA', 'root@epita.fr', 'b4d3d15b35c2b26f00cb84113d7ce87b4fc20e91659e9d39e2f8dd86944a508f', NULL, '82.227.10.101');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Act_In`
--
ALTER TABLE `Act_In`
  ADD CONSTRAINT `Act_In_ibfk_1` FOREIGN KEY (`id_person`) REFERENCES `Person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Act_In_ibfk_2` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Episode`
--
ALTER TABLE `Episode`
  ADD CONSTRAINT `Episode_ibfk_1` FOREIGN KEY (`id_season`) REFERENCES `Season` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Film`
--
ALTER TABLE `Film`
  ADD CONSTRAINT `Film_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `History`
--
ALTER TABLE `History`
  ADD CONSTRAINT `History_ibfk_1` FOREIGN KEY (`subscription_id`) REFERENCES `Subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Is_Type`
--
ALTER TABLE `Is_Type`
  ADD CONSTRAINT `Is_Type_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Production`
--
ALTER TABLE `Production`
  ADD CONSTRAINT `Production_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Production_ibfk_2` FOREIGN KEY (`id_person`) REFERENCES `Person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Realisation`
--
ALTER TABLE `Realisation`
  ADD CONSTRAINT `Realisation_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Realisation_ibfk_2` FOREIGN KEY (`id_person`) REFERENCES `Person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Screening`
--
ALTER TABLE `Screening`
  ADD CONSTRAINT `Screening_ibfk_1` FOREIGN KEY (`id_film`) REFERENCES `Film` (`id_media`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Screening_ibfk_2` FOREIGN KEY (`id_theater`) REFERENCES `Theater` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Season`
--
ALTER TABLE `Season`
  ADD CONSTRAINT `Season_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `Serie` (`id_media`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Serie`
--
ALTER TABLE `Serie`
  ADD CONSTRAINT `Serie_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Subscription`
--
ALTER TABLE `Subscription`
  ADD CONSTRAINT `Subscription_ibfk_2` FOREIGN KEY (`id_offer`) REFERENCES `Offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Subscription_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Teaser`
--
ALTER TABLE `Teaser`
  ADD CONSTRAINT `Teaser_ibfk_1` FOREIGN KEY (`id_media`) REFERENCES `Media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
